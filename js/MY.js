jQuery.exists = function (selector) {
    return ($(selector).length > 0);
};

(function($) {

    if($.exists('.MY-menu-toggle')){
        $(".MY-menu-toggle-hide").hide();

        $('.MY-menu-toggle').click(function(){
            $('.MY-menu-toggle-hide').slideToggle();
        });
    }

    if($.exists("#image-gallery")){
        $('#image-gallery').lightSlider({
            gallery:true,
            item:1,
            slideMargin: 0,
            speed:500,
            auto:false,
            loop:false,
            keyPress:true,
            onSliderLoad: function() {
                $('#image-gallery').removeClass('cS-hidden');
            }
        });
    }

    if($.exists("#searchopen")){
        $("#searchopen").click(function () {
            $("#searchactive").toggle("slow");
            $("#showLeft").toggle("invise");
        });
    }

    if($.exists("#slider1")){
        $('#slider1').tinycarousel();
        $('.vdblocks a').magnificPopup({
            type: 'iframe',
            closeOnContentClick: false,
            closeBtnInside: false,
            mainClass: 'mfp-fade',
            iframe: {
                patterns: {
                    youtube: {
                        index: 'youtube.com/',
                        id: 'v=',
                        src: 'https://www.youtube.com/embed/%id%?autoplay=1'
                    }
                }
            }
        });
    }

    if($.exists("#container")){
        var $container = $('#container');
        // init
        $container.packery({
            itemSelector: '.item',
            gutter: 20
        });
    }


    if($.exists(".bxslider")){
        $('.bxslider').bxSlider();
    }



    if($.exists(".col-xs-9-320")){
        function window_resize(){
            if( $("html").width() < 619 ){
                $('.col-xs-3-320').prependTo('.col-xs-9-320');
            }
        }
        window_resize();
    }



    if($.exists("#accordeon li div")){
        $("#accordeon li > div").click(function() {
            $(this).next().slideToggle(300);
            $(window).resize();
            if( $(this).find('.red-triangle').hasClass('red-triangle-reverse') ){
                $(this).find('.red-triangle').removeClass('red-triangle-reverse');
            } else {
                $(this).find('.red-triangle').addClass('red-triangle-reverse');
            }
        });
        $('.checktoggle').click(function(event){
            event.stopPropagation();
        })
    }





    var menuLeft = document.getElementById('cbp-spmenu-s1'),
        menuRight = document.getElementById('cbp-spmenu-s2'),
        menuTop = document.getElementById('cbp-spmenu-s3'),
        menuBottom = document.getElementById('cbp-spmenu-s4'),
        showLeft = document.getElementById('showLeft'),
        closeLeft = document.getElementById('closeLeft'),
        showRight = document.getElementById('showRight'),
        showTop = document.getElementById('showTop'),
        showBottom = document.getElementById('showBottom'),
        showLeftPush = document.getElementById('showLeftPush'),
        showRightPush = document.getElementById('showRightPush'),
        body = document.body;

    showLeft.onclick = function () {
        classie.toggle(this, 'active');
        classie.toggle(menuLeft, 'cbp-spmenu-open');
        disableOther('showLeft');
    };

    closeLeft.onclick = function () {
        classie.toggle(this, 'active');
        classie.toggle(menuLeft, 'cbp-spmenu-open');
        disableOther('closeLeft');
    };


    showRight.onclick = function () {
        classie.toggle(this, 'active');
        classie.toggle(menuRight, 'cbp-spmenu-open');
        disableOther('showRight');
    };
    showTop.onclick = function () {
        classie.toggle(this, 'active');
        classie.toggle(menuTop, 'cbp-spmenu-open');
        disableOther('showTop');
    };
    showBottom.onclick = function () {
        classie.toggle(this, 'active');
        classie.toggle(menuBottom, 'cbp-spmenu-open');
        disableOther('showBottom');
    };
    showLeftPush.onclick = function () {
        classie.toggle(this, 'active');
        classie.toggle(body, 'cbp-spmenu-push-toright');
        classie.toggle(menuLeft, 'cbp-spmenu-open');
        disableOther('showLeftPush');
    };

    closeLeftPush.onclick = function () {
        classie.toggle(this, 'active');
        classie.toggle(body, 'cbp-spmenu-push-toright');
        classie.toggle(menuLeft, 'cbp-spmenu-open');
        disableOther('closeLeftPush');
    };

    showRightPush.onclick = function () {
        classie.toggle(this, 'active');
        classie.toggle(body, 'cbp-spmenu-push-toleft');
        classie.toggle(menuRight, 'cbp-spmenu-open');
        disableOther('showRightPush');
    };

    function disableOther(button) {
        if (button !== 'showLeft') {
            classie.toggle(showLeft, 'disabled');
        }
        if (button !== 'closeLeft') {
            classie.toggle(closeLeft, 'disabled');
        }
        if (button !== 'showRight') {
            classie.toggle(showRight, 'disabled');
        }
        if (button !== 'showTop') {
            classie.toggle(showTop, 'disabled');
        }
        if (button !== 'showBottom') {
            classie.toggle(showBottom, 'disabled');
        }
        if (button !== 'showLeftPush') {
            classie.toggle(showLeftPush, 'disabled');
        }
        if (button !== 'closeLeftPush') {
            classie.toggle(closeLeftPush, 'disabled');
        }
        if (button !== 'showRightPush') {
            classie.toggle(showRightPush, 'disabled');
        }
    }




})(jQuery);